from bs4 import BeautifulSoup
import urllib2
from urlparse import *
import re
import string

PITCHFORK_BASE='http://pitchfork.com'
AUTHOR_SEARCH_BASE='http://pitchfork.com/search/?query='

"""
1) start with top 50 list
	get album urls

2) get authoer, 
	check dict,
	add all of artist's entries to author author_frontier

3) for each page: 
	album name
	artist

	score
	
	artist
	
	PF ID


TODO:
	-add more albums, and make an environment variable. right now, only 9 per each
	-add accolades
"""

class pitchfork_index:
	def __init__(self):
		self.top_50_frontier=set()
		self.author_frontier=set()
		self.review_index_frontier=set()
		self.author_lookup={}
		self.artist_lookup={}
		self.meta_data_dictionary={}  #k:review-postfix	 v:(album_names ,	artist_name  ,  reviewer_name  , date   , score)
		self.review_bags={} #k:postfix    v: ()
		self.term_dict={} #k: term   v: order in which it enters, indexed by 0

		stop_file=open('stoplist.txt', 'r')
		init_stopList=set()
		for line in stop_file:
			line= line.rstrip('\n')
			line= line.rstrip('\r')			
			init_stopList.add(line)
		stop_file.close()
		init_stopList.remove('')
		self.stop_list = init_stopList #creates a Set


#test_url='http://pitchfork.com/reviews/albums/17407-voodoo/'
#test_url='http://pitchfork.com/features/staff-lists/9017-the-top-50-albums-of-2012/'


############
#step 1, get seeds
###########
	def scrape_top_50(self):
		url_base='http://pitchfork.com/features/staff-lists/9017-the-top-50-albums-of-2012/'
		for i in range(1,6):
			print i
			cur_url=url_base + str(i) + '/'
			print cur_url
			url_list=self.get_top_50_links(cur_url)
			print url_list
			for u in url_list:
				self.top_50_frontier.add(u)

	def get_top_50_links(self, seed_url):
		page_soup=self.make_soup(seed_url)
		album_names=page_soup('span', class_='list-title')
		rev_links=[n.find_all('a') for n in album_names]
		flat_links=[i for o in rev_links for i in o]
		flat_links=[l.get('href') for l in flat_links]
		flat_links=set(flat_links)
		flat_links_clean=self.clean_link_list(list(flat_links))
		return flat_links_clean


############
##step2, get authors
#########

	def get_authors_names(self):
		"""

		"""
		if not self.top_50_frontier:
			print 'no review seeds.'
			print 'first run scrape_top_50'
			return
		for rev_url in self.top_50_frontier:
			rev_author=self.extract_author(rev_url)
			print rev_author
			self.author_frontier.add(rev_author)
			self.author_lookup[rev_author]=set()


	def extract_author(self, rev_url):
		page_soup=self.make_soup(rev_url)
		rev_meta=page_soup.find('ul', class_='review-meta')
		return rev_meta.h4.address.text


##############
###step 3, get reviews
###############
	def extract_reviews_by_author(self):
		if not self.author_frontier:
			print 'no author seeds'
			print 'first, run scrape_top_50, then get_authors_names'
			return
		for author in self.author_frontier:
			f_name, l_name=author.split()
			auth_url= AUTHOR_SEARCH_BASE + f_name + '+' + l_name
			author_soup=self.make_soup(auth_url)
			album_grid=author_soup.find('ul', class_='object-grid')
			raw_links=[l.get('href') for l in album_grid.find_all('a')]
			fixed_links=[self.fix_query_links(rl) for rl in raw_links]
			fixed_links=[fl for fl in fixed_links if fl]
			for fl in fixed_links:
				self.review_index_frontier.add(fl)

	def fix_query_links(self, raw_url):
		p_url=urlparse(raw_url)
		pattern='^/reviews/albums'
		if not re.search(pattern, p_url.path):
			print 'bad_link, or bad regular expression:'
			print p_url.path
			return None
		if p_url.netloc:
			return raw_url
		return urljoin(PITCHFORK_BASE, raw_url)


################
#####step 4, extract review contents
################
	def process_reviews(self):
		for r_url in self.review_index_frontier:
			rev_soup=self.make_soup(r_url)
			album_post_fix=self.get_post_fix(r_url)
			self.extract_meta_data(rev_soup, album_post_fix)
			self.index_review_text(rev_soup, album_post_fix)

	def index_review_text(self, rev_soup, album_post_fix):
		article=rev_soup.find('div', class_='editorial').text
		clean_article_list=self.clean_an_article(article)
		cur_review_bag={}
		for term in clean_article_list:
			if term in self.term_dict:
				term_id=self.term_dict[term]
			else:
				term_id=len(self.term_dict) #indexes from 0
				self.term_dict[term]=term_id
			
			if term_id in cur_review_bag:
				cur_freq=cur_review_bag[term_id] + 1
			else:
				cur_freq=1
			cur_review_bag[term_id]=cur_freq
		self.review_bags[album_post_fix]=cur_review_bag





	def clean_an_article(self, article_string):
		"""
		takes raw, unicode string of all article
		returns list of asci encoded strings
		performs:
			-stop word filtering
			-lower casing
			-spliting on spaces and hyphens
			-removal of punctuation (not ' though ')
		"""	
		article= article_string.encode('ascii', 'ignore')
		article=article.strip()
		article=article.lower()
		article_list=article.split()
		article_list=[element.split('-') for element in article_list]
		article_list=[inner for outer in article_list for inner in outer]
		exclude=set(string.punctuation)
		exclude.remove('\'')
		clean_article_list=[''.join(ch for ch in word if ch not in exclude) for word in article_list]
		clean_article_list=[word for word in clean_article_list if word not in self.stop_list and word.isalpha()]
		return clean_article_list



	def extract_meta_data(self, rev_soup, album_post_fix):
		print ('extracting from: ' + str(album_post_fix))
		rev_meta=rev_soup.find('ul', class_='review-meta')
		reviewer_name=rev_meta.h4.address.text
		artist_name=rev_meta.h1.text
		album_name=rev_meta.h2.text
		score=rev_meta.find('span', class_= 'score').text
		date=rev_meta.find('span', class_='pub-date').text
			#    v= (album_names ,	artist_name  ,  reviewer_name  , date   , score)
		cur_meta=(album_name, artist_name, reviewer_name, date, score)
		print cur_meta
		self.meta_data_dictionary[album_post_fix]=cur_meta
	



###########
### utils
##########
	def get_post_fix(self, full_url):
		album_post_fix=full_url.split('/')
		album_post_fix=[u for u in album_post_fix if u]
		return album_post_fix[-1]
			

	def make_soup(self, seed_url):
		test_url_file=urllib2.urlopen(seed_url)
		page_string=test_url_file.read()
		test_url_file.close()
		return BeautifulSoup(page_string)
		
	def clean_link_list(self, raw_url_list):
		base='http://pitchfork.com/reviews/albums/'
		pattern='^' +base 
		flat_links_clean=[l for l in raw_url_list if re.search(pattern, l)]
		return flat_links_clean

RUN=True
if RUN:
	pi=pitchfork_index()
	pi.scrape_top_50()
	print "getting author names"
	pi.get_authors_names()
	print 'extracting reviews for each author...'
	pi.extract_reviews_by_author()
	pi.process_reviews()

'''
test_soup=BeautifulSoup(test_page_string)
article=test_soup.find('div', class_='editorial')
linked_articles=article.find_all('a')
rev_meta=test_soup.find('ul', class_='review-meta')


#<h4>

'''